Dr. Malika Kohli pursued her degree in dentistry through studies in her native India and at Boston University. She followed-up with several years of additional specialization in periodontics and dental implantation therapies. She has been in practice as a periodontist and general dentist since 2008.

Address: 2407 Columbia Pike, #280, Arlington, VA 22204, USA

Phone: 571-312-4111
